--- Checking Translation

local S

if minetest.get_translator ~= nil then
    S = minetest.get_translator(minetest.get_current_modname())
else
    S = function(str)
        return(str)
    end
end

--- Registering Armor

minetest.register_tool("brights:bright_chestplate", {
	description = S("Bright Chestplate"),
	inventory_image = "3d_armor_chestplate_bright.png",
    groups = {armor_torso=1, armor_heal=18, armor_use=20, armor_fire=1, not_in_creative_inventory=1},
    armor_groups = {fleshy =20},
    damage_groups = {cracky=2, snappy=1, level=3},
    wear = 0,
})

minetest.register_tool("brights:bright_leggings", {
	description = S("Bright Leggings"),
	inventory_image = "3d_armor_leggings_bright.png",
    groups = {armor_legs=1, armor_heal=18, armor_use=20, armor_fire=1, not_in_creative_inventory=1},
    armor_groups = {fleshy =20},
    damage_groups = {cracky=2, snappy=1, level=3},
    wear = 0,
})

minetest.register_tool("brights:bright_boots", {
	description = S("Bright Boots"),
	inventory_image = "3d_armor_boots_bright.png",
    groups = {armor_feet=1, armor_heal=18, armor_use=20,
        physics_speed=1.5, physics_jump=1, armor_fire=1, armor_fire=1, not_in_creative_inventory=1},
    armor_groups = {fleshy =15},
    damage_groups = {cracky=2, snappy=1, level=3},
    wear = 0,
})

minetest.register_tool("brights:bright_helmet", {
	description = S("Bright Helmet"),
	inventory_image = "3d_armor_helmet_bright.png",
    groups = {armor_head=1, armor_heal=18, armor_use=20, armor_fire=1, not_in_creative_inventory=1},
    armor_groups = {fleshy =15},
    damage_groups = {cracky=2, snappy=1, level=3},
    wear = 0,
})


--- Registering Recipes

minetest.register_craft({
	output = "brights:bright_chestplate",
	recipe = {
		{"brights:brightsubstance", "", "brights:brightsubstance"},
		{"brights:brightsubstance", "brights:brightsubstance", "brights:brightsubstance"},
		{"brights:brightsubstance", "brights:brightsubstance", "brights:brightsubstance"},
	},
})

minetest.register_craft({
	output = "brights:bright_leggings",
	recipe = {
		{"brights:brightsubstance", "brights:brightsubstance", "brights:brightsubstance"},
		{"brights:brightsubstance", "", "brights:brightsubstance"},
		{"brights:brightsubstance", "", "brights:brightsubstance"},
	},
})

minetest.register_craft({
	output = "brights:bright_boots",
	recipe = {
		{"brights:brightsubstance", "", "brights:brightsubstance"},
		{"brights:brightsubstance", "", "brights:brightsubstance"},
	},
})

minetest.register_craft({
	output = "brights:bright_helmet",
	recipe = {
		{"brights:brightsubstance", "brights:brightsubstance", "brights:brightsubstance"},
		{"brights:brightsubstance", "", "brights:brightsubstance"},
		{"", "", ""},
	},
})