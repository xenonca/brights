--[[
____________________.___  ________  ___ _______________________
\______   \______   \   |/  _____/ /   |   \__    ___/   _____/
 |    |  _/|       _/   /   \  ___/    ~    \|    |  \_____  \ 
 |    |   \|    |   \   \    \_\  \    Y    /|    |  /        \
 |______  /|____|_  /___|\______  /\___|_  / |____| /_______  /
        \/        \/            \/       \/                 \/ 
]]--

--- Checking for translation

local S
local sleep = 0

if minetest.get_translator ~= nil then
    S = minetest.get_translator(minetest.get_current_modname())
else
    S = function(str)
        return(str)
    end
end

--- Checking for 3D Armor/Shields

if minetest.get_modpath("3d_armor") then
	dofile(minetest.get_modpath("brights").."/brights_armor.lua")
end

if minetest.get_modpath("shields") then
	dofile(minetest.get_modpath("brights").."/brights_shields.lua")
end

--- Overriding Coral Skeleton

minetest.override_item("default:coral_skeleton", {drop = {
    max_items = 1,
    items = {
			{items = {'brights:brightsubstance'}, 
				rarity = tonumber(minetest.settings:get("brightsubstance_rarity")) or 50, -- default set to 50
				tools = {'default:pick_mese', 'default:pick_diamond', 'brights:brightpick', 'ethereal:pick_crystal'}},
            {items = {'default:coral_skeleton'}},
            }
}})

--- Registering Items/Tools

minetest.register_craftitem("brights:brightsubstance", {
    description = S("Bright Substance"),
    inventory_image = "brightsubstance.png",
    tiles = "brightsubstance.png",
	groups = {not_in_creative_inventory=1},
	is_visible = false,
	range = 7,
})

minetest.register_node("brights:brightblock", {
	description = S("Bright Block"),
	tiles = {"brightblock.png"},
	is_ground_content = false,
	groups = {not_in_creative_inventory=1, cracky=2, level=5},
})

minetest.register_node("brights:brightblock_r", {
	description = S("Bright Block Reinforced"),
	tiles = {"brightblock_r.png"},
	is_ground_content = false,
	groups = {not_in_creative_inventory=1, cracky=1, level=5},
})

minetest.register_tool("brights:brightsword", {
	description = S("Bright Sword"),
	inventory_image = "brightsword.png",
	groups = {not_in_creative_inventory=1},
	range = 7,
	tool_capabilities = {
		full_punch_interval = 0.5,
		max_drop_level=1,
		groupcaps={
			snappy={times={[1]=1.70, [2]=0.70, [3]=0.25}, uses=70, maxlevel=5},
		},
		damage_groups = {fleshy=11},
	},
	sound = {breaks = "default_tool_breaks"},
})

minetest.register_tool("brights:brightpick", {
    description = S("Bright Pickaxe"),
	inventory_image = "brightpick.png",
	groups = {not_in_creative_inventory=1},
	range = 7,
    tool_capabilities = {
        full_punch_interval = 0.8,
		max_drop_level=3,
        groupcaps= {
            cracky={times={[1]=1.80, [2]=0.80, [3]=0.40}, uses=70, maxlevel=5}
        },
        damage_groups = {fleshy=7},
       },
    sound = {breaks = "default_tool_breaks"},
})

minetest.register_tool("brights:brightaxe", {
    description = S("Bright Axe"),
	inventory_image = "brightaxe.png",
	groups = {not_in_creative_inventory=1},
	range = 7,
    tool_capabilities = {
        full_punch_interval = 0.7,
		max_drop_level=3,
        groupcaps={
			choppy={times={[1]=2.00, [2]=0.80, [3]=0.40}, uses=70, maxlevel=5},
		},
		damage_groups = {fleshy=7},
	},
	sound = {breaks = "default_tool_breaks"},
})

minetest.register_tool("brights:brightshovel", {
	description = S("Bright Shovel"),
	inventory_image = "brightshovel.png",
	wield_image = "brightshovel.png^[transformR90",
	groups = {not_in_creative_inventory=1},
	range = 7,
	tool_capabilities = {
		full_punch_interval = 0.9,
		max_drop_level=1,
		groupcaps={
			crumbly = {times={[1]=1.10, [2]=0.50, [3]=0.30}, uses=70, maxlevel=5},
		},
		damage_groups = {fleshy=6},
	},
	sound = {breaks = "default_tool_breaks"},
})

if minetest.get_modpath("farming") then
    farming.register_hoe("brights:brighthoe", {
		description = S("Bright Hoe"),
		inventory_image = "brighthoe.png",
		max_uses = 1000,
		groups = {not_in_creative_inventory = 1},
		range = 7,
		damage_groups = {fleshy=7},
	})
	
end

minetest.register_tool("brights:brightstick", {
	description = S("Bright Stick"),
	inventory_image = "brightstick.png",
	wield_image = "brightstick.png^[transformR90",
	groups = {not_in_creative_inventory=1},
	range = 10,
	tool_capabilities = {
		full_punch_interval = 10.0,
		max_drop_level=1,
		groupcaps={
			snappy={uses=70},
		},
		damage_groups = {fleshy=1},
	},
	sound = {breaks = "default_tool_breaks"},
	on_use = function(itemstack, player, pointed_thing)
		if pointed_thing.above == nil then
			return
		end
		if sleep == 7  then
			return
		end
			minetest.add_particlespawner({
				amount = 1000,
				time = 7,
				minpos = {x = tonumber(pointed_thing.above.x)+5, y = tonumber(pointed_thing.above.y), z = tonumber(pointed_thing.above.z)+5},
				maxpos = {x = tonumber(pointed_thing.above.x)-5, y = tonumber(pointed_thing.above.y), z = tonumber(pointed_thing.above.z)-5},
				minvel = {x = -0, y = 0, z = -0},
				maxvel = {x = 3, y = 3, z = 3},
				minacc = {x = 1, y = 1, z = 1},
				maxacc = {x = -1, y = -1, z = -1},
				minexptime = 5,
				maxexptime = 10,
				minsize = 5,
				maxsize = 20,
				texture = "brightness.png",
				collisiondetection = false,
				minetest.after(7, function()
					sleep = 0
				end)
			})
			sleep = 7
		end,

})
--- Registering Recipes

minetest.register_craft({
	output = "brights:brightsword 1",
	recipe = {
		{"brights:brightsubstance"},
		{"brights:brightsubstance"},
		{"default:obsidian_shard"}
	}
})

minetest.register_craft({
	output = "brights:brightpick 1",
	recipe = {
		{"brights:brightsubstance","brights:brightsubstance","brights:brightsubstance"},
		{"","default:obsidian_shard",""},
		{"","default:obsidian_shard",""}
	}
})

minetest.register_craft({
	output = "brights:brightaxe 1",
	recipe = {
		{"brights:brightsubstance","brights:brightsubstance",""},
		{"brights:brightsubstance","default:obsidian_shard",""},
		{"","default:obsidian_shard",""}
	}
})

minetest.register_craft({
	output = "brights:brightshovel 1",
	recipe = {
		{"", "brights:brightsubstance", ""},
		{"", "default:obsidian_shard", ""},
		{"", "default:obsidian_shard", ""}
	}
})

minetest.register_craft({
	output = "brights:brighthoe 1",
	recipe = {
		{"brights:brightsubstance", "brights:brightsubstance", ""},
		{"", "default:obsidian_shard", ""},
		{"", "default:obsidian_shard", ""}
	}
})

minetest.register_craft({
	output = "brights:brightstick 1",
	recipe = {
		{"", "", "brights:brightsubstance"},
		{"", "default:obsidian_shard", ""},
		{"default:obsidian_shard", "", ""}
	}
})

minetest.register_craft({
	output = "brights:brightblock",
	recipe = {
		{"brights:brightsubstance", "brights:brightsubstance", "brights:brightsubstance"},
		{"brights:brightsubstance", "brights:brightsubstance", "brights:brightsubstance"},
		{"brights:brightsubstance", "brights:brightsubstance", "brights:brightsubstance"},
	}
})

minetest.register_craft({
	output = "brights:brightsubstance 9",
	recipe = {
		{"brights:brightblock"},
	}
})

minetest.register_craft({
	output = "brights:brightblock_r",
	recipe = {
		{"default:steel_ingot", "", "default:steel_ingot"},
		{"", "brights:brightblock", ""},
		{"default:steel_ingot", "", "default:steel_ingot"},
	}
})

minetest.register_craft({
	output = "brights:brightblock",	
	recipe = {
		{"brights:brightblock_r"},
	}
})
